<?php   
include('../include/menu.php');    
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Liste des fournisseurs</title>
        <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Les fournisseurs :</h1>
<?php
include("../include/connexion.php");
/**
 * Page qui affiche la liste de tout les fournisseurs
 */
$requete = 'SELECT c.libelle
    

            , f.code
            , f.nom
            , f.contact
            , v.codepostal
            , v.nom as ville
            FROM fournisseur f
            , civilite c
            , ville v
            where f.civilite = c.code
            and f.ville = v.code';
?>

    <table class="table table-striped table-hover">
        
        <thead>
            <tr>
                <th>Code</th>
                <th>Contact</th>
                <th>civilite</th>
                <th>Nom</th>
                <th>Code Postal</th>
                <th>Ville</th>
            </tr>
            
            
        </thead>
        
        <tbody>     
<?php

try {
    foreach($bdd->query($requete) as $ligne) {
        echo '<tr class = "clickable-row table-hover" data-href="fournisseur.php?id=' . $ligne['code'] . '">';
        echo '<td>' . $ligne['code'] . '</td>';
        echo '<td>' . $ligne['nom'] . '</td>';
        echo '<td>' . $ligne['libelle'] . '</td>';
        echo '<td>' . $ligne['contact'] . '</td>';
        echo '<td>' . $ligne['codepostal'] . '</td>';
        echo '<td>' . $ligne['ville'] . '</td>';
        echo "</tr>\n";
    }
    
} catch (PDOException $e) {
    echo 'Erreur !: ' . $e->getMessage() . '<br>';
    die();
}

?>
                </tbody>
                </table>
<div class="container">
 <form method="post" action="fournisseur.php">
 <div class="form-group row float-right">
 <input type="submit" class="btn btn-primary" name="Nouveau"
value="Nouveau">
 </div>
 </form>
</div>
            </table>
        </div>
        <script src="../node_modules/jquery/dist/jquery.js"></script>
 <script>
 $(document).ready(function($) {
 $(".clickable-row").click(function() {
 window.location = $(this).data("href");
 });
 });
 </script>

    </body>
</html>

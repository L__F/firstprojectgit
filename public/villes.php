<?php
session_start();
require_once('../include/connexion.php');
require_once('../include/menu.php');
require_once('../include/fonction.php');
echo afficheMessages();

$id = (isset($_GET['id'])) ? $_GET['id'] : 0;

if ($id == 0) {
    header("Location:$url/listeVille.php");
    die();
}

var_dump($_POST);

if (isset($_POST['Modifier'])) {

    if (empty($nom) || strlen($nom) < 2) {
        $_SESSION['MSG_KO'] .= "Le nom de la ville est obligatoire et doit contenir au minimum 2 caractères<br>";
    }

    if (empty($codepostal) || strlen($codepostal) != 5) {
        $_SESSION['MSG_KO'] .= "Le code postal doit contenir 5 chiffres<br>";
    }

    $array = ['France', 'france', 'Andorre', 'andorre', 'Monaco', 'monaco'];
    if (!in_array($pays, $array)) {
        $_SESSION['MSG_KO'] .= "Le pays doit être dans la liste « France », « Andorre » ou « Monaco »<br>";
    }
    $_SESSION['MSG_OK'] = "Modification bien enregistrée";
    if (!isset($_SESSION['MSG_KO'])) {
        try {
            $bdd = new PDO("mysql:host=$host;dbname=$base", $env['user'], $env['password']);
            $requete = $bdd->prepare('update ville
                set nom = :nom,
                codepostal = :codepostal,
                pays = :pays
                WHERE code = :code');
            $resultat = $requete->execute(array(
                ':nom' => $_POST['nom'],
                ':codepostal' => $_POST['codepostal'],
                ':pays' => $_POST['pays'],
                ':code' => $_POST['code']
            ));
            $_SESSION['MSG_OK'] = "Modification bien enregistrée";
        } catch (PDOException $e) {
            $_SESSION['MSG_KO'] = "Erreur lors de la modification";
            echo "Erreur : " . $e->getMessage();
        }
    }
}
try {
    $requete = $bdd->prepare('select code, nom, codepostal, pays from ville where code = ?');
    $requete->execute(array($id));
    $ville = $requete->fetch();
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

// Condition pour vérifier si le bouton Annuler a été cliqué
if (isset($_POST['Annuler'])) {
    // Redirection vers la liste des fournisseurs
    header('Location: listevilles.php');
    exit(); // Terminer le script pour éviter toute exécution ultérieure
}
//suppression d'un fournisseur
if (isset($_POST['Supprimer'])){
    try{
      $requete = $bdd->prepare('delete from ville where code = ?');
      $requete->execute(array($_POST['code']));
      $_SESSION['MSG_OK'] = "supression bien enregistrée";
      header("Location: ./listevilles.php");
      exit;
    }catch (PDOException $e){
      print "Erreur !:" .$e->getMessage() . "<br/>";
      echo afficheMessages($_SESSION['MSG_KO'] = "Modification non enregistrée");
      die();
    }
  }
?>

<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ville : <?php echo $ville['nom']; ?></title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <h1>Ville : <?php echo $ville['nom']; ?></h1>
        <form method="post">
            <input type="hidden" class="form-control" id='code' name='code' value="<?php echo $ville['code'] ?>">

            <div class="formulaire">
                <label for="examplenom" class="form-label">Nom :</label>
                <input type="nom" class="form-control" name="nom" id="examplenom" value="<?php echo $ville['nom'] ?>">
            </div>

            <div class="formulaire">
                <label for="examplecodepostal" class="form-label">Code postal :</label>
                <input type="codepostal" class="form-control" name="codepostal" id="examplecodepostal" value="<?php echo $ville['codepostal'] ?>">
            </div>

            <div class="formulaire">
                <label for="examplepays" class="form-label">Pays :</label>
                <input type="pays" class="form-control" name="pays" id="examplepays" value="<?php echo $ville['pays'] ?>">
            </div>
            <div class="form-group row float-right">
                <input type="submit" class="btn btn-default" name="Annuler" value="Annuler">
                <input type="submit" class="btn btn-primary" name="Modifier" value="Modifier">
            </div>
            <br> <button type="submit" name="Supprimer" class="btn btn-danger confirm">Supprimer</button></br>
        </form>
    </div>
</body>
<script src="../node_modules/jquery/dist/jquery.js"></script>
<script>
    $(function() {
        $('.confirm').click(function() {
            return window.confirm("Êtes-vous sur ?");
        });
    });
</script>

</html>
<?php

/**
 * Fonction qui génère un input type select avec toutes les villes
 * @param $id id du select
 * @return code HTML à afficher
 */
function selectVille($id, $code)
{
    global $bdd;
    $retour = "<select class=\"form-control\" id=\"$id\" name=\"$id\">\n";
    try {
        $requete = 'select code, nom from ville';
        foreach ($bdd->query($requete) as $ligne) {
            if ($ligne['code'] == $code) {
                $retour .= "<option selected='selected' value=" . $ligne['code'] . ">" . $ligne['nom'] . "</option>";
            } else {
                $retour .= '<option value=' . $ligne['code'] . '>' . $ligne['nom'] . '</option>' . "\n";
            }
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    $retour .= "</select>";
    return $retour;
}
?>
<?php

function selectCivilite($id, $code)
{
    global $bdd;
    $retour =  "<select class=\"form-control\" id=\"$id\" name=\"$id\">\n";
    try {
        $requete = 'select code, libelle from civilite';
        foreach ($bdd->query($requete) as $ligne) {
            if ($ligne['code'] == $code) {
                $retour .= "<option selected='selected' value=" . $ligne['code'] . ">" . $ligne['libelle'] . "</option>";
            } else {
                $retour .= '<option value=' . $ligne['code'] . '>' . $ligne['libelle'] . '</option>' . "\n";
            }
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    $retour .= "</select>";
    return $retour;
}


function menuActif($menu)
{
    $ecran = basename($_SERVER['SCRIPT_FILENAME'], ".php");

    if ($menu == $ecran) {
        return "active";
    } else {
        return "";
    }
}

/*function afficheMessages2()
{
    // initialise la variable 
    global $bdd;
    $retour = '';
    $nom = isset($_POST['nom']) ? trim($_POST['nom']) : '';

    if (strlen($nom) >= 3) {

        if (!empty($_POST['adresse1'])) {
            $_SESSION['MSG_OK'] = "Modification bien enregistrée";

            if (isset($_POST['Modifier'])) {
                // vérifie si la variable de session est définie
                
                if (!empty($_SESSION['MSG_OK'])) {

                    // ajouter la chaine html
                    $retour .= '<div class="alert alert-success">' . htmlspecialchars($_SESSION['MSG_OK']) . '</div>' . "\n";

                    // supprime la variable de session
                    unset($_SESSION['MSG_OK']);
                }
            }
        } else {
            $_SESSION['MSG_KO'] = "Erreur entrée une adresse !";
            if (isset($_POST['Modifier'])) {

                // vérifie si la variable de session est définie
                if (!empty($_SESSION['MSG_KO'])) {

                    // ajouter la chaine html
                    $retour .= '<div class="alert alert-danger">' . htmlspecialchars($_SESSION['MSG_KO']) . '</div>' . "\n";

                    // supprime la variable de session
                    unset($_SESSION['MSG_KO']);
                }
            }
        }

        /* Vérification de l'unicité du nom du fournisseur
        $requete = $bdd->prepare('SELECT COUNT(*) AS cpt FROM fournisseur WHERE nom = ?');
        $requete->execute(array($nom));
        $compteur = $requete->fetch();
        if ($compteur['cpt'] == 1) {
            $retour .= "le nom (" . $_POST['nom'] . ") est déjà pris<br />";
        }
        var_dump($bdd);
        $requete = $bdd->prepare('select count(*) as cpt from fournisseur where nom = ? ');
    $requete->execute(array($_POST['nom']));
    $compteur = $requete->fetch();
    if ($compteur['cpt'] == 1) {
        $_SESSION['MSG_KO'] .= "le nom (" . $_POST['nom'] . ") est déjà pris<br />";
    }
    } else {
        $_SESSION['MSG_KO'] = "ERREUR, 3 caractères minimum dans le nom !";
        if (isset($_POST['Modifier'])) {

            // vérifie si la variable de session est définie
            if (!empty($_SESSION['MSG_KO'])) {

                // ajouter la chaine html
                $retour .= '<div class="alert alert-danger">' . htmlspecialchars($_SESSION['MSG_KO']) . '</div>' . "\n";

                // supprime la variable de session
                unset($_SESSION['MSG_KO']);
            }
        }
    }

    

    echo $retour;
}*/

function afficheMessages()
{
    $retour = '';
    if (!empty($_SESSION['MSG_OK'])) {
        $retour .= '<div class="alert alert-success">' . $_SESSION['MSG_OK'] .
            '</div>' . "\n";
        unset($_SESSION['MSG_OK']);
    } elseif (!empty($_SESSION['MSG_KO'])) {
        $retour .= '<div class="alert alert-danger">' . $_SESSION['MSG_KO'] .
            '</div>' . "\n";
        unset($_SESSION['MSG_KO']);
    }
    return $retour;
}
